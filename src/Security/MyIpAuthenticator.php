<?php

namespace App\Security;

use Symfony\Component\Security\Core\Security;

class MyIpAuthenticator
{
    private $security;

    public function __construct(Security $security)
    {
      $this->security = $security;
    }

    public function supports(Request $request)
    {
      if ($this->security->getUser()) {
          return false;
      }

      // the user is not logged in, so the authenticator should continue
      return true;
    }
}


<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[]
     */
    public function findAllGreaterThanDate($date): array
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT p
            FROM App\Entity\Article p
            WHERE p.date > :date
            ORDER BY p.date ASC'
        )->setParameter('date', $date);

        // returns an array of Product objects
        return $query->getResult();
    }

    public function removeArticle($articles)
    {
        $em = $this->getEntityManager();

        foreach ($articles as $article) {
            $em->remove($article);
        }
        $em->flush();
    }
}

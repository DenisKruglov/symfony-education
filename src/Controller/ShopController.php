<?php

// src/Controller/DefaultController.php
namespace App\Controller;

use App\Entity\ShopOrder;
use App\Form\OrderFormType;
use App\Repository\ProductRepository;
use App\Repository\ShopCartRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\ShopCart;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ShopController extends AbstractController
{
    private $session;

    private $list;

    /**
     * ShopController constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->session->start();
        $this->session->set('flag', '1');
    }


    /**
     * @Route("/shop", name="product_list")
     */
    public function productList(ProductRepository $products)
    {
        $entities = $products->findAll();

        return $this->render('shop.html.twig', [
            'products' => $entities,
        ]);
    }

    /**
     * @Route("/shop/cart"), name="shop_cart_overview")
     */
    public function ShopCartOverview(ShopCartRepository $shopCart, ProductRepository $products): Response
    {
        $sessionId = $this->session->getId();
        $orderList = $shopCart->findBy(array('sessionId' => $sessionId));

        foreach ($orderList as $order) {
            $this->list[] = $products->findOneBy(array('id' => $order->getProductItem()));
        }

        return $this->render('shop-cart.html.twig', [
            'products' => $this->list,
            'order' => $orderList,
        ]);
    }

    /**
     * @Route("/shop/order"), name="shop_cart_overview")
     *
     * @return Request
     * @return Response
     */
    public function shopOrder(ProductRepository $products, ShopCartRepository $shopCart, Request $request, EntityManagerInterface $em): Response
    {
        // just setup a fresh $task object (remove the example data)
        $shopOrder = new ShopOrder();

        $form = $this->createForm(OrderFormType::class, $shopOrder);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated]
            $shopOrder = $form->getData();

            if($shopOrder instanceof ShopOrder) {
                $sessionId = $this->session->getId();
                $orderListCarts = $shopCart->findBy(array('sessionId' => $sessionId));
                foreach ($orderListCarts as $orderListCart) {
                    $orderListProducts = $products->findOneBy(array('id' => $orderListCart->getProductItem()));
                }

//                $shopOrderitem = $shopOrder->setProductItem($orderList[0]);
//                dump($orderListProducts);
//                die;
//                $em->persist($orderListProducts);
//                $em->flush();
                $shopOrder->setStatus(ShopOrder::STATUS_NEW_ORDER);
                $shopOrder->setSessionId($sessionId);
                $shopOrder->setProductItem($orderListProducts);

                // ... perform some action, such as saving the task to the database
                // for example, if Task is a Doctrine entity, save it!
                // $entityManager = $this->getDoctrine()->getManager();
                $em->persist($shopOrder);
                $em->flush();
                $this->session->migrate();
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('index/shop-order.html.twig', [
            'title' => 'Ordering',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/shop/{productId}", name="product_overview")
     *
     * @param $productId
     * @return Response
     */
    public function productItem(ProductRepository $products, int $productId): Response
    {
        $entity = $products->findOneBy(array('id' => $productId));

        return $this->render('product.html.twig', [
            'product' => $entity,
            'id' => $entity->getId(),
//            'rate' => $commentRepository->findBy(['product' => $entity], ['createdAt' => 'DESC']),
        ]);
    }

    /**
     * @Route("/shop/cart/add/{productId<\d+>}", name="shopCartAdd")
     *
     * @param $productItem
     * @return Response
     */
    public function shopCartAdd (ProductRepository $products, EntityManagerInterface $em, int $productId): Response
    {
        $productItem = $products->findOneBy(array('id' => $productId));
        $sessionId = $this->session->getId();

        $shopCart = (new ShopCart())
            ->setProductItem($productItem)
            ->setCount(1)
            ->setSessionId($sessionId)
        ;
        $em->persist($shopCart);
        $em->flush();

        return $this->redirectToRoute('product_overview', ['productId' => $productId]);
    }
}
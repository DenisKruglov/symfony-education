<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Twig\Environment;
use Doctrine\ORM\EntityManagerInterface;

class ArticleController extends AbstractController
{
     /**
     * @Route("/blog", name="blog_list")
     */
    public function list(Environment $twig, CommentRepository $commentRepository, ArticleRepository $article): Response
    {
        return new Response($twig->render('blog.html.twig'));
    }

    /**
     * @Route("/blog/{articleId}", name="article_show", methods="GET", requirements={"article"="\d+"})
     */
    public function show(ArticleRepository $article, Environment $twig, CommentRepository $commentRepository, int $articleId): Response
    {

        $entity = $article->findOneBy(array('id' => $articleId));

        return new Response($twig->render('post-details.html.twig', [
            'article' => $entity,
            'comments' => $commentRepository->findBy(['article' => $entity], ['createdAt' => 'DESC']),
            'user_id' => $commentRepository->findBy(['article' => $entity], ['createdAt' => 'DESC']),
        ]));
    }
}

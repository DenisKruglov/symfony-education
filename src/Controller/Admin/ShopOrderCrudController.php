<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\ShopCart;
use App\Entity\ShopOrder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class ShopOrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Shoporder::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('User orders')
            ->setEntityLabelInPlural('User orders')
            ->setSearchFields(['user', 'phone', 'email']);
//            ->setDefaultSort(['createdAt' => 'DESC']);
    }

//    public function configureFilters(Filters $filters): Filters
//    {
//        return $filters
//            ->add(EntityFilter::new('productItem'))
//            ;
//    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('user');
        yield EmailField::new('email');
        yield TextareaField::new('phone');
        yield AssociationField::new('productItem');
//        yield TextField::new('photoFilename')
//            ->onlyOnIndex()
//        ;

//        $createdAt = DateTimeField::new('createdAt')->setFormTypeOptions([
//            'html5' => true,
//            'years' => range(date('Y'), date('Y') + 5),
//            'widget' => 'single_text',
//        ]);
//        if (Crud::PAGE_EDIT === $pageName) {
//            yield $createdAt->setFormTypeOption('disabled', true);
//        } else {
//            yield $createdAt;
//        }
    }
}

<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class CreateUserCommand extends Command
{

    private $requirePassword;
    private $em;
//    private $metadata;

    // the name of the command
    protected static $defaultName = 'app:remove-article';

    public function __construct(EntityManagerInterface $em, bool $requirePassword = false)
    {
        $this->em = $em;
        $this->requirePassword = $requirePassword;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // description php bin/console list
            ->setDescription('Remove old news')

            // description with the "--help" option
            ->setHelp('Remove old news with a date older than 1 year');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $date = new \DateTime('@'.strtotime('now'));
        $metadata = new ClassMetadata(Article::class);

        $date = date("i-h-s", strtotime('-1 year'));

        $articles = $this->em->getRepository(Article::class)
            ->findAllGreaterThanDate($date);

        if (!empty($articles)) {
            $articles = $this->em->getRepository(Article::class)
                ->removeArticle($articles);

            // outputs lines to the console (adding "\n" at the end of each line)
            $output->writeln([
                'Remove old news',
            ]);

            // outputs a message followed by a "\n"
            $output->writeln('Whoa!');
            // outputs a message without adding a "\n" at the end of the line
            $output->write('You are about to ');
            $output->write('remove an article.' . "\n");
        } else {
            $output->writeln([
                'No old news with a date older than 1 year',
            ]);
        }

        return Command::SUCCESS;
    }


}